# Running the application

Clone the project

Using ssh
```shell
git clone git@gitlab.com:webpanel/backend.git
```
Using https
```shell
git clone https://gitlab.com/webpanel/backend.git
```

Create ur docker image
```shell
docker build -t de.kxmpetentes.web.panel/backend:1.2.2 .
```

Create a docker container
```shell
docker run --name web-panel-backend -p 8081:8081 -v /home/web-panel/config/:config -v /home/web-panel/images/:images de.kxmpetentes.web.panel/backend
```

---
A Postgres SQL database is required.

Default Login for the first admin account: admin/admin
