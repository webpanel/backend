# build stage
FROM gradle:jdk17-alpine as build-stage
WORKDIR /app
COPY src ./
COPY build.gradle ./
COPY gradle ./
RUN gradle bootJar

# production stage
FROM openjdk:18-alpine as production-stage
COPY --from=build-stage /build/libs/backend-1.2.2.jar /app
EXPOSE 8081
CMD ["java", "-jar", "backend-1.2.2"]
