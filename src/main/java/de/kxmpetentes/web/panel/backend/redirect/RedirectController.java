package de.kxmpetentes.web.panel.backend.redirect;

import de.kxmpetentes.web.panel.core.entity.ShortenUrl;
import de.kxmpetentes.web.panel.core.service.ShortenUrlService;
import de.kxmpetentes.web.panel.core.utils.RedirectUtil;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class RedirectController {

  private final ShortenUrlService shortenUrlService;

  @Autowired
  public RedirectController(ShortenUrlService shortenUrlService) {
    this.shortenUrlService = shortenUrlService;
  }

  @GetMapping("/s/")
  public ResponseEntity<?> handleNoShortId(HttpServletRequest request) {
    return RedirectUtil.getHomeRedirect(request);
  }

  @GetMapping("/s/{shortId}")
  public ResponseEntity<?> handleRedirect(@PathVariable String shortId, HttpServletRequest request)
      throws URISyntaxException {
    ShortenUrl shortenUrl = shortenUrlService.getByRedirectUrl(shortId);
    if (shortenUrl == null) {
      return RedirectUtil.getHomeRedirect(request);
    }

    URI uri = new URI(shortenUrl.getOriginalUrl());
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setLocation(uri);
    return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).headers(httpHeaders).build();
  }

}