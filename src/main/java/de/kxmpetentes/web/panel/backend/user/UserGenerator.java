package de.kxmpetentes.web.panel.backend.user;

import de.kxmpetentes.web.panel.core.Role;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.entity.UserSetting;
import de.kxmpetentes.web.panel.core.service.UserRepository;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserGenerator implements ApplicationRunner {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserGenerator(UserRepository userRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public void run(ApplicationArguments args) {
    if (userRepository.count() != 0) {
      return;
    }

    User user = new User();
    user.setName("admin");
    user.setRoles(Set.of(Role.values()));
    user.setUsername("admin");
    user.setHashedPassword(passwordEncoder.encode("admin"));
    user.setApiToken(UUID.randomUUID());

    UserSetting userSettings = user.getUserSettings();
    userSettings.setUploaderPageTitle("Web Panel | Image");
    userSettings.setHexColorCode("#89b4fa");
    userSettings.setLanguage(Locale.ENGLISH);

    userRepository.save(user);
  }
}
