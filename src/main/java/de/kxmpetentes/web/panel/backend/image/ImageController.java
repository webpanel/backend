package de.kxmpetentes.web.panel.backend.image;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import de.kxmpetentes.web.panel.core.entity.User;
import de.kxmpetentes.web.panel.core.entity.UserSetting;
import de.kxmpetentes.web.panel.core.service.UploadedImageService;
import de.kxmpetentes.web.panel.core.service.UserRepository;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@RestController
public class ImageController {

  private static final Logger LOGGER = LogManager.getLogger();
  private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
  private static final char[] INVISIBLE_SPACES = new char[]{
      '\u200c',
      '\u200d',
      '\u200e',
      '\u200f',
  };

  private final File folder = new File("images/");

  private final UserRepository userRepository;
  private final UploadedImageService uploadedImageService;

  @Autowired
  public ImageController(UserRepository userRepository, UploadedImageService uploadedImageService) {
    this.userRepository = userRepository;
    this.uploadedImageService = uploadedImageService;
    if (!folder.exists()) {
      if (!folder.mkdirs()) {
        LOGGER.error("Could not create Image Folder");
      }
    }
  }

  @GetMapping(value = "/image/{imageName}", produces = MediaType.IMAGE_PNG_VALUE)
  public @ResponseBody void getImage(@PathVariable String imageName, HttpServletResponse response) throws IOException {
    UploadedImage image = uploadedImageService.getImageByName(imageName);
    if (image == null) {
      return;
    }

    File imageFile = new File(String.format("%s/%s.png", folder.getPath(), imageName));
    InputStream fileInputStream = new FileInputStream(imageFile);
    IOUtils.copy(fileInputStream, response.getOutputStream());
  }

  @PostMapping(value = "/upload/image")
  public ResponseEntity<?> handleImageUpload(HttpServletRequest request) {
    MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    Iterator<String> fileNames = multipartRequest.getFileNames();

    MultipartFile file = multipartRequest.getFile(fileNames.next());
    if (file == null) {
      return new ResponseEntity<>("Uploaded file is null", HttpStatus.BAD_REQUEST);
    }

    String apiToken = request.getHeader("token");
    User user = userRepository.findByApiToken(UUID.fromString(apiToken));
    if (user == null) {
      return ResponseEntity.badRequest().body("No valid api token");
    }

    UserSetting settings = user.getUserSettings();
    String colorCode = settings.getHexColorCode();
    String pageTitle = settings.getUploaderPageTitle();

    String zeroWidthString = RandomStringUtils.random(100, INVISIBLE_SPACES);
    String fileName = RandomStringUtils.random(16, true, true);
    UploadedImage image = new UploadedImage();

    image.setFileName(fileName);
    image.setUploader(user.getName());
    image.setHexColor(colorCode);
    image.setDomain(pageTitle);
    image.setImageId(zeroWidthString);

    try {
      byte[] bytes = file.getBytes();
      BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(bytes));
      ImageIO.write(bufferedImage, "png", new File(folder, fileName + ".png"));
      uploadedImageService.insertOrUpdate(image);
    } catch (IOException e) {
      return new ResponseEntity<>("Can't save file", HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(GSON.toJson(image), HttpStatus.OK);
  }
}
